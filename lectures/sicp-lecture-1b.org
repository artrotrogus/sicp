#+TITLE: SICP lectures - 1B
#+AUTHOR: MZuppichin
#+STARTUP: hidestars
#+OPTIONS: ^:nil
#+STARTUP: inlineimages
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="tufte.css">

* Constructing spells
  :LOGBOOK:
  - Note taken on [2021-02-02 Tue 23:45] \\
    Got to 36.48 of MIT 6.001 lesson 1B
  :END:
  A programmer builds spells out of procedures in order to get some effect on the world.
  One has to understand the relationship between spells he writes and the features of the world he aims to control.
  
  It's important to name things properly in order to have control over them!

  We proceed to get insight and intuition on the problem by making analogies and by exploiting some *perturbation methods*, i.e. changing the program a little bit how may affect output?

** Substitution Model
   Go for Engineering models, in which reality is strongly simplified.
   Don't over-focus on how a real machine may implement what we do, rather simplify your models in order to understand them.
   
   Substitution model is just a model and an approximation of how a machine works. It's kind of 1st order Taylor expansion of the machine behavior and its validity is restricted to some specific use cases. 

   
*** Rules for application evaluation
    - Evaluate the operator to get a procedure
    - Evaluate the operands to get arguments
    - Apply the procedure to the arguments by copying the body of procedure substituting the arguments supplied for the formal parameters
    - Evaluate the resulting body

    ... It's a form of Beta Reduction!!
    #+begin_src racket :lang racket
      ((λ (x y) (+ x y)) 4 6)
      =
      (λ (x y) (+ x y)) calculated at x=4, y=6
      =
      (+ 4 6)
      =
      10
    #+end_src

    Normal Order or apply order are conventions which lead to same result as far as this is concerned. Check the examples in the book which are overly informative.
    
*** Rules for conditionals
    - Evaluate <predicate>
    - If predicate is TRUE evaluate <consequent>
    - If procedure is FALSE pick <alternative>


  
* Recursive definitions
  Now we list two recursive definitions that let two totally different shapes in time/memory.

** Example: Linear Iteration
   
*** Code Example
      #+begin_src racket :lang racket
	(define (++ x y)
	  (let ([inc (λ (x) (+ x 1))]
		[dec (λ (x) (- x 1))])
	    (if (= 0 x)
		y
		(++ (dec x) (inc y)))))

	;; test the definition on 3,4
	(++ 3 4)
	(++ 2 5)
	(++ 1 6)
	(++ 0 7)
	7
      #+end_src
    
*** Complexity
    In the above piece of code, see the number of steps (i.e. vertical line) as *time* whereas the /length/ of objects as indication of [memory] *space* to be passed around.
    The above example runs in O(x) time (proportional to its argument x), O(1) space.
    The overall algorithm can be executed in constant space regardless of the machine! (of course abstracting from representation of numeric datatypes)
      
*** Beureucracy Example
    Pass a card with two numbers to a person, asking him to add them and returning the result to you.

    The guy performs (dec x) (inc y) and passes another card to another person by telling to return result back to you at the end of the process.
    
    The guys keep doing stuff up to when returning the end value back to you.
*** Crash recovery
    If I suddently kill the proces at the middle can I recover?
    Yes! All the information needed to end up the computation is inside the variables defined in the procedure.

    Iteration has all its state in explicit variables. 

** Example: Linear Recursion
   
*** Code example
     #+begin_src racket :lang racket
       (define (+++ x y)
	 (let ([inc (λ (x) (+ x 1))]
	       [dec (λ (x) (- x 1))])
	   (if (= x 0)
	       y
	       (inc (+++ (dec x) y)))))

       ;; test on 3,4
       (+++ 3 4)
       (inc (+++ 2 4))
       (inc (inc (+++ 1 4)))
       (inc (inc (inc (+++ 0 4))))
       (inc (inc (inc 4)))
       (inc (inc 5))
       (inc 6)
       7
     #+end_src
     
*** Complexity
    The above piece of code executes in O(x) time, O(x) space (take a look at bell-like shape of =inc= which grows and decreases as computation goes on).

*** Beureucracy Example
    More people employed with respect to Linear Iteration!
    
    You give a card with two numbers to friend A asking for sum.
    
    Friend A performs (dec x) and passes the card to friend B, A knowing he'll need to add 1 to the result he gets back from B.
    
    Go on with this logic, each one knows his successor and the action he has to perform on the result he gets back.
*** Crash recovery
    If I suddently kill the proces at the middle can I recover?
    Nope! I lose information on previous states returning values, I can no longer reconstruct the computation.
    
    Recursion, in addition to storing information in variables, hides information under the table pertaining to the computer, i.e. what has been deferred for later computation.
** Example: Fibonacci the good old way
   This is an example of a bad performing algorithm.
   #+begin_src racket :lang racket
     (define (fib n)
       (if (< n 2)
	   n
	   (+ (fib (- n 1))
	      (fib (- n 2)))))
   #+end_src
   
   This usual definition builds a tree in which left branch goes like =(fib (- n 1))= whereas right branch goes like =(fib (+ n 1))=... I have lots of repetition!
   The same subtrees are computed twice! e.g. =(fib 4)= requires at its first step to compute =(fib 3)= adn =(fib 2)=... but in the first subtree I have =(fib 2)= again!
   Hence this algorithm grows exponentially in time: O(fib(n)).
   
   The space complexity of the algorithm is related to the path in the tree (depth of the tree) and goes like O(n).
